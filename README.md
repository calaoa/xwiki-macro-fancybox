# XWiki Fancybox Gallery Macro

This macro allows to display a set of images in a zoomable and responsive gallery using the [Fancybox library](https://fancyapps.com/fancybox/3/) library.

* Project Lead: [slauriere](http://www.xwiki.org/xwiki/bin/view/XWiki/slauriere)
* Communication: [Forum / Chat / Mailing List](https://dev.xwiki.org/xwiki/bin/view/Community/Discuss)
* Minimal XWiki version supported: 8.4.10
* License: GPLv3 (same license as Fancybox)

Test

